//console.log("Hello World");

// ES6 Updates -- is one of the latest versions of writing JS and one of the major update to js. let/const

// Exponent Operator
const firstNum = 8 ** 2;
console.log(firstNum); //64

const secondNum = Math.pow(8,2);
console.log(secondNum);


let string1 = "fun";
let string2 = "Bootcamp";
let string3 = "Coding";
let string4 = "Javascript";
let string5 = "Zuitt";
let string6 = "Learning";
let string7 = "Love";
let string8 = "I";
let string9 = "is";
let string10 = "in";


// Mini Activity1
// const sentence1 = string8.concat(string7,string4,string6);
// console.log(sentence1);

// const sentence2 = string5.concat(string2,string6,string9,string1);
// console.log(sentence2);

// "",'' - string literals
// Template Literals -- allow us to create strings `` and easily embed JS expressions in it.

let sentence1 = `${string8} ${string7} ${string5} ${string3} ${string2}!`;
console.log(sentence1);

// ${} -- used to embed JS expressions when creating strings using Template Literals


let name = "John";

// Pre-template Literal string
// ("") or ('')
let message = "Hello" + name + '! Welcome to programming!';

// Strings using Template Literals
// (``) backticks
message = `Hello ${name}! Welcome to programming.`

// Multi-line using Template Literals

const anotherMessage = `
${name} attended a math competition.
He won it by solving the problem 8 ** 2 with the solution of ${firstNum}.
`
console.log(anotherMessage);


let dev = {
	name: "Peter",
	lastName: "Parker",
	occupation: "web developer",
	income: 50000,
	expenses: 60000
}
console.log(`${dev.name} is a ${dev.occupation}.`);
console.log(`His income is ${dev.income} and expenses at ${dev.expenses}. His current balance is ${dev.income - dev.expenses}.`);


const interestRate = .1;
const principal = 1000;

console.log(`The interest on your savings account is ${principal * interestRate}`);


// Array Destructing -- allow us to unpack elements in arrays into distinct variables

const fullName = ["Juan","Dela","Cruz"];
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It is nice to see you!`);


// Array Destructuring
const [firstName, middleName, lastName] = fullName;

console.log(firstName);
console.log(middleName);
console.log(lastName);

console.log(`Hello ${firstName} ${middleName} ${lastName}! It is nice to see you!`);


// Object Destructuring -- allow us to unpack properties of objects into distinct varaiables.

const person = {
	givenName: "Jane",
	maidenName: "Dela",
	familyName: "Cruz"
};

// Pre-Object Destructuring -- use dot or square notation
console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName); 

//  Object Destructuring
const {givenName, maidenName, familyName} = person;

console.log(givenName);
console.log(maidenName);
console.log(familyName);


// Arrow functions -- compact alternative syntax to traditional functions

const hello = () => {
console.log("Hello World");
}

// const hello = function hello(){
// 	console.log("Hello World");
// }
hello();


// Pre-Arrow Function and Template Literals
// Arrow function
const printFullName = (firstName,middleInitial,lastName) => {
	console.log(`${firstName} ${middleInitial} ${lastName}`);
}
printFullName("John","D","Smith");


// Arrow function with loops
// pre-arrow functiom
const students = ["John","Jane","Judy"];

students.forEach(function(student){
	console.log(`${student} is a student`);
})


// Arrow function
students.forEach((student) =>{
	console.log(`${student} is a student`)
	});


// Implicit Return Statement -- there are instances when you can omit the "return" statement

// pre-arrow function
// const add = (x,y) => {
// 	return x + y;
// }


const add = (x,y) => x+y;

// let total = add(1,2);
// console.log(total);


// Mini Activity2

const substract = (x,y) => x-y;
let total1 = substract(1,2);

const multiply = (x,y) => x*y;
let total2 = multiply(1,2);

const divide = (x,y) => x/y;
let total3 = divide(1,2);

console.log(total1);
console.log(total2);
console.log(total3);


// Default Function Argument Value -- provide a default argument value if none is provided when the function

const greet =(name = 'User') => {
	return `Good Evening, ${name}`;
}
console.log(greet());
console.log(greet("John"));


// Class-Based Object Blueprint -- allow creation/installation of objects using classes as blueprints

// Creating a class

class Car {
	constructor(brand,name,year){    //blueprint
		this.brand = brand;
		this.name = name;
		this.year = year;
	}
}

// Instantiate an object
// the "new" operator creates/instantiate a new object with the given arguments as the value of its properties

const myCar = new Car();
console.log(myCar); //undefined

myCar.brand = "Ford";
myCar.name = "Everest";
myCar.year = "1996";
console.log(myCar);


const myNewCar = new Car("Toyota","Vios",2021);
console.log(myNewCar);


// Mini Activity3

class characters{ 
	constructor(name,role,strength,weakness){
		this.name = name;
		this.role = role;
		this.strength = strength;
		this.weakness =weakness;
	}
}

const newCharacter = new characters("Superman","Hero","LaserEye","Cryptonite");
console.log(newCharacter);