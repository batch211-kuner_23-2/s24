//console.log("Hello World");

const getCube = 2 ** 3;

printGetCube = `The cube of 2 is ${getCube}`;
console.log(printGetCube);

const address = ["Prk.27","Brgy.San Vicente","Panabo City","Davao del Norte",8105];

console.log(`I live at ${address[0]} , ${address[1]} , ${address[2]} , ${address[3]} , ${address[4]}`);

const animal = {
	nameOfAnimal: "Lolong",
	typeOfAnimal: "Crocodile",
	species: "salt water",
	weight: "1075 kgs.",
	measurement: "20 ft. 3 in."
};

const {nameOfAnimal,typeOfAnimal,species,weight,measurement} = animal;

console.log(`${nameOfAnimal} was a ${species} ${typeOfAnimal}. He weight at ${weight} with measurement of ${measurement}`);


const numbers = [1,2,3,4,5];
numbers.forEach((num) => console.log(num));

const reduceNumber = [1,2,3,4,5];
const reducer = (accu, curr) => accu + curr;

console.log(reduceNumber.reduce(reducer));


class Dog {
	constructor(name,age,breeds){    //blueprint
		this.name = name;
		this.age = age;
		this.breeds = breeds;
	}
}

const myDog = new Dog("Bogart",2,"Pug");
console.log(myDog);